//
//  API.swift
//  Atividade_iOS
//
//  Created by COTEMIG on 02/04/1444 AH.
//

import UIKit

class API: UITableView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBOutlet weak var imagem: UIImageView!
    
    @IBOutlet weak var titulo: UILabel!

    @IBOutlet weak var descricao: UILabel!
    
}
