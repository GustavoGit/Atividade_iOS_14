//
//  ViewController.swift
//  Atividade_iOS
//
//  Created by COTEMIG on 02/04/1444 AH.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftUI

struct artist: Decodable{
    let name: String
    let actor: String
    let image: String
}


class ViewController: UIViewController, UITableViewDataSource {
    
    var artistList: [artist]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return artistList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "API", for: indexPath) as! MyCell
                let actor = artistList![indexPath.row]
                
                cell.nome.text = actor.name
                cell.ator.text = actor.actor
                cell.imagem.kf.setImage(with: URL(string: actor.image))
        
                return cell
    }
    
    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        data()
        TableView.dataSource = self
    }
    
    private func data(){
            AF.request("https://hp-api.herokuapp.com/api/characters")
                .responseDecodable(of: [artist].self){
                response in
                    if let actor_list = response.value{
                        self.artistList = actor_list
                }
                    self.TableView.reloadData()
            }
        }

    
}

